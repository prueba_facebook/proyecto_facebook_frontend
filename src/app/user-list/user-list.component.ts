import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { UserListService } from '../services/user-list.services';
import PerfectScrollbar from 'perfect-scrollbar';
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {

  public users = [];
  @ViewChild('table') table: ElementRef;
  constructor(private _userListService: UserListService) { }

  ngOnInit(): void {
    this.userListService();
  }

  ngAfterViewInit() {
    new PerfectScrollbar( this.table.nativeElement);
  }

  protected setUsers(users) {
    this.users = users;
  }

  protected userListService() {
    this._userListService.handle().subscribe({
      next: this.userListServiceOk.bind(this),
      error: this.userListServiceErr.bind(this)
    });
  }

  protected userListServiceOk(response) {
    let data = response.data();
    this.setUsers(data);
    this.emailMap(this.users);
  }

  protected userListServiceErr(err) {
    let data = err.data();
    console.log('userListServiceErr', data);
  }

  protected emailMap(users) {
    console.log("Comenzando la impresion************************************************");
    console.log(users.map(user=>user.email));
    console.log("Finalizando la impresion************************************************");
  }

}
