import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { users } from "./contracts/user-list";
import { ApiGateway } from "../api-gateway/http/api-gateway";
import { ApiGatewayCommand } from "../api-gateway/http/api-gateway-command";

@Injectable({ providedIn: 'root' })

export class UserListService {
    constructor(private apiGateway: ApiGateway) { }

    handle() {
        let command = new ApiGatewayCommand({
            url: environment.users.list.url,
            method: ApiGatewayCommand.METHOD_DELETE,
            params: {},
            headers: {},
            debug: environment.users.list.debug,
            response: this.apiGateway.responseSuccessFake(users)
        });

        return this.apiGateway.execute(command);
    }

}