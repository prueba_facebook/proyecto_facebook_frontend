export class ApiGatewayResponse {

	private _data;
	private _status;

	static make(data, status) {
		return new ApiGatewayResponse(data, status);
	}

	constructor(data, status) {
		this._data = data;
		this._status = status;
	}

	data() { return this._data; }

	status() { return this._status; }

	hasError() {
		return this._status > 200;
	}

	hasErrorAndEqual(status) {
		return this.hasError() && this._status == status;
	}

} 