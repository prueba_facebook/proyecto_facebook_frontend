import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { ApiGatewayCommand } from './api-gateway-command';
import { ApiGatewayResponse } from './api-gateway-response';
import { map, catchError } from 'rxjs/operators'
import { throwError, Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class ApiGateway {

  constructor(private http: HttpClient) { }

  public execute(command: ApiGatewayCommand): Observable<ApiGatewayResponse> {
    return command.debug()
      ? command.responseFake()
      : this.asyncAction(command);
  }

  private asyncAction(command) {
    let action = this[command.method()].bind(this);
    let observable = action(command);
    return observable.pipe(
      map((response: HttpResponse<any>) => {
        let apiGatewayResponse = ApiGatewayResponse.make(response.body, response.status);
        return apiGatewayResponse;
      }),
      catchError((errorResponse: HttpErrorResponse) => {
        let apiResponseError = ApiGatewayResponse.make(
          errorResponse.error,
          errorResponse.status
        );

        return throwError(apiResponseError);
      })
    );
  }

  private get(command) {
    return this.http.get(command.url(), command.configGet());
  }

  private post(command) {
    return this.http.post(command.url(), command.params(), command.configPost());
  }

  private put(command) {
    return this.http.put(command.url(), command.params(), command.configPost());
  }

  responseSuccessFake(dummyData) {
    return this.responseFake(dummyData, 200);
  }

  responseErrorLogicFake(dummyData) {
    return this.responseFake(dummyData, 400);
  }

  responseErrorServerFake(dummyData) {
    return this.responseFake(dummyData, 500);
  }

  responseFake(data, status) {
    return new ApiGatewayResponse(data, status);
  }
}

