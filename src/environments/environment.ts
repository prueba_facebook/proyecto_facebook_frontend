import { ApiRouter, ON, OFF } from "./env-utils";
const backend = ApiRouter("localhost", "some-api", OFF);

export const environment = {
  production: false,
  users: {
    list: backend.route('list')
  }
};